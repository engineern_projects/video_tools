from pathlib import Path
from typing import Tuple, Optional, List

from video_tools.tools.utils import run_command

mkvmerge_path = r"C:\Program Files\MKVToolNix\mkvmerge.exe"


def __run_command(args: str, path: Path) -> Tuple[str, str]:
    args = '"' + str(mkvmerge_path) + '" ' + args
    return run_command(args, path)


def split(mkv_path: Path, chapters: str, output_name: str) -> Tuple[Optional[List[Path]], str]:
    if mkv_path.name == output_name:
        return None, "Failed to split mkv for {0} with error: {1}.".format(str(mkv_path),
                                                                           "mkv_path name cannot be the same as "
                                                                           "output_name")

    output, error = __run_command(
        '"' + str(mkv_path) + '" --split chapters:' + str(chapters) + ' -o "' + str(output_name) + '"',
        mkv_path.parent)
    if output == "":
        return None, "Failed to split mkv for {0} with error: {1}.".format(str(mkv_path), error)

    mkv_paths = list(mkv_path.parent.glob("*.mkv"))

    for item in mkv_paths:
        if item.name == mkv_path.name:
            mkv_paths.remove(item)
    if len(mkv_paths) == 0:
        return None, "Failed to split mkv for {0} with error: {1}.".format(str(mkv_path),
                                                                           "Failed to find MKV Output")

    return mkv_paths, ""


def merge(current_path: Path, mkv_inputs: List[str], output_name: str) -> Tuple[Optional[Path], str]:
    output_name_path = current_path / output_name
    if str(output_name_path) in mkv_inputs:
        return None, "Failed to merge mkv for {0} with error: {1}.".format(str(output_name),
                                                                           "mkv_path name cannot be the same as "
                                                                           "output_name")

    file_list = '"' + '" + "'.join(mkv_inputs) + '"'
    args = f'-o "{str(output_name_path)}" {file_list}'
    output, error = __run_command(args, current_path)
    if output == "":
        return None, "Failed to generate title count for {0} with error: {1}.".format(str(output_name), error)

    mkv_path = current_path / output_name
    if not mkv_path.exists():
        return None, "Failed to split mkv for {0} with error: {1}.".format(str(mkv_path),
                                                                           "Failed to find MKV Output")

    return mkv_path, ""
