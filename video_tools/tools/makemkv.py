from pathlib import Path
from typing import Tuple, Optional

from video_tools.tools.utils import run_command

mkv_path = r"C:\Program Files (x86)\MakeMKV\makemkvcon64.exe"


def __run_command(args: str, path: Path) -> Tuple[str, str]:
    args = '"' + str(mkv_path) + '" ' + args
    return run_command(args, path)


def get_title_count(iso_path: Path) -> Tuple[int, Optional[str]]:
    output, error = __run_command('info iso:"' + str(iso_path) + '"', iso_path.parent)
    if output == "":
        return -1, "Failed to get title count for {0} with error: {1}.".format(str(iso_path), error)

    if output.find("Failed to open disc") != -1:
        return -1, "Failed to get title count for {0} with error: {1}.".format(str(iso_path), "Failed to open disc")

    if output.find("This application version is too old") != -1:
        return -1, "Failed to get title count for {0} with error: {1}.".format(str(iso_path), "Update your MakeMKV")

    for item in output.split("\n"):
        if "Total " in item:
            total = int(item[6:-8])

            if total > 100:
                total = 100

            return total, None


def generate_title(iso_path: Path, title_id: int) -> Tuple[Optional[Path], str]:
    dir_path = iso_path.parent

    output, error = __run_command('mkv iso:"' + str(iso_path) + '" ' + str(title_id) + ' "' + str(dir_path) + '"',
                                  dir_path)
    if output == "":
        return None, "Failed to generate title count for {0} with error: {1}.".format(str(iso_path), error)

    if output.find("Failed to open disc") != -1:
        return None, "Failed to generate title count for {0} with error: {1}.".format(str(iso_path),
                                                                                      "Failed to open disc")
    if output.find("This application version is too old") != -1:
        return None, "Failed to get title count for {0} with error: {1}.".format(str(iso_path), "Update your MakeMKV")

    mkv_paths = list(dir_path.glob("*.mkv"))
    if len(mkv_paths) == 0:
        return None, "Failed to generate title count for {0} with error: {1}.".format(str(iso_path),
                                                                                      "Failed to find MKV Output")

    return mkv_paths[0], ""
