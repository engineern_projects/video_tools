import os
import signal
import subprocess
import time
from pathlib import Path
from typing import Tuple


def run_command(args: str, temp_dir: Path) -> Tuple[str, str]:
    try:
        output_string = ""
        log_path = temp_dir / "logfile.txt"
        logfile = open(log_path, 'w+', newline='')
        p = subprocess.Popen(args, stderr=subprocess.STDOUT, stdout=subprocess.PIPE)

        def close():
            logfile.close()
            if p.poll() is None:
                os.kill(p.pid, signal.SIGTERM)
                p.kill()
            time.sleep(0.1)

        try:
            last_position = 0
            buffer = b''

            def write(line):
                nonlocal last_position, output_string
                decoded_line = line.decode('ascii', errors='replace').encode('ascii', errors='replace').decode()
                logfile.seek(last_position)
                logfile.truncate()

                output_string += decoded_line
                logfile.write(decoded_line)
                logfile.flush()

                if decoded_line.endswith('\r\n'):
                    last_position = logfile.tell()

            while True:
                byte = p.stdout.read(1)

                if byte == b'' and p.poll() is not None:
                    write(buffer)
                    break
                elif byte == b'':
                    continue

                buffer += byte

                if len(buffer) >= 2:
                    if buffer[-2] == 13 and buffer[-1] == 10:
                        write(buffer)
                        buffer = b''
                    elif buffer[-2] == 13:
                        temp_buffer = buffer[:-1]
                        write(temp_buffer)
                        buffer = buffer[-1:]

        finally:
            close()

        return output_string, ""
    except Exception as e:
        return "", e
