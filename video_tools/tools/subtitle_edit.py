from pathlib import Path
from typing import Tuple, List, Optional

from video_tools.tools.utils import run_command

subtitle_edit_path = r"C:\Program Files\Subtitle Edit\SubtitleEdit.exe"


def __run_command(args: str, path: Path) -> Tuple[str, str]:
    args = '"' + subtitle_edit_path + '" ' + args
    return run_command(args, path)


def generate_subtitles(mkv_path: Path) -> Tuple[Optional[List[Path]], str]:
    output, error = __run_command('/convert "' + str(mkv_path) + '" SubRip', mkv_path.parent)
    if output == "":
        return None, "Failed to generate subtitles for {0} with error: {1}.".format(str(mkv_path), error)

    if "No subtitle tracks in Matroska file" in output:
        return [], ""

    subtitle_paths = list(mkv_path.parent.glob("*.srt"))
    if len(subtitle_paths) == 0:
        return None, "Failed to generate subtitles for {0} with error: {1}.".format(str(mkv_path),
                                                                                    "Failed to find SRT Output")

    return subtitle_paths, ""
