from pathlib import Path
from typing import Tuple, Optional

from video_tools.info.common import Resolution
from video_tools.tools.utils import run_command

handbrake_path = r"C:\Program Files\HandBrake\HandBrakeCLI.exe"

presets = {
    Resolution.r480p: '--preset "General/HQ 480p30 Surround" --crop 0:0:0:0 --optimize',
    Resolution.r720p: '--preset "General/HQ 720p30 Surround" --crop 0:0:0:0 --optimize',
    Resolution.mobile: '--preset "General/HQ 720p30 Surround" --crop 0:0:0:0 --optimize --vb 3200',
    Resolution.r1080p: '--preset "General/HQ 1080p30 Surround" --crop 0:0:0:0 --optimize',
    Resolution.tv: '--preset "General/HQ 1080p30 Surround" --crop 0:0:0:0 --optimize --vb 7200',
    Resolution.r2160p: '--preset "Devices/Apple 2160p60 4K HEVC Surround" --crop 0:0:0:0 --optimize --rate 30 '
                       '--align-av'
}


def __run_command(args: str, path: Path) -> Tuple[str, str]:
    args = '"' + str(handbrake_path) + '" ' + args
    return run_command(args, path)


def generate_file(mkv_path: Path, output_name: str,
                  resolution: Resolution,
                  new_resolution: Resolution) -> Tuple[Optional[Path], str]:
    if resolution not in presets.keys() or new_resolution not in presets.keys():
        return None, "Failed to generate file count for {0} with error: {1}.".format(str(mkv_path),
                                                                                     "Failed to find resolution given")

    if resolution != new_resolution:
        output_path = Path(mkv_path.parent / (output_name + "@" + new_resolution.name + ".mp4"))
    else:
        output_path = Path(mkv_path.parent / (output_name + ".mp4"))

    output, error = __run_command(presets[new_resolution] + ' -i "' + str(mkv_path) + '" -o "' + str(output_path) + '"',
                                  mkv_path.parent)
    if output == "":
        return None, "Failed to generate file count for {0} with error: {1}.".format(str(mkv_path), error)

    if not output_path.exists():
        return None, "Failed to generate file count for {0} with error: {1}.".format(str(mkv_path),
                                                                                     "Failed to generate output")

    return output_path, ""
