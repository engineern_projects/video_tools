import re
import shutil
import time
from pathlib import Path
from typing import List, Tuple, Dict

from tqdm import tqdm

from video_tools.info import utils
from video_tools.info.data import Mp4Item, Mp4Corrections
from video_tools.info.subtitles import valid_suffixes
from .util import check_path, print_with_delay


def generate_parser(sub_parser):
    parser = sub_parser.add_parser("file_organizer", description="")
    parser.add_argument(
        "--backup_dir",
        type=check_path,
        help="Backup directory path.",
        required=True
    )
    parser.add_argument(
        "--video_dir",
        type=check_path,
        help="Video directory path.",
        required=True
    )
    parser.set_defaults(func=main)


def main(args):
    print_with_delay("Scanning for data.json and status.json files.")
    json_pair_list, json_err = utils.get_info_pairs(args.backup_dir)
    if json_err:
        raise Exception(json_err)

    error_list: List[str] = utils.check_pairs_for_errors(json_pair_list)
    video_directory = args.video_dir

    print_with_delay("Generate list of videos from data.json files.")
    global_generated_list: List[str] = []
    generated_list: Dict[str, Dict[Tuple[str, int], List[Tuple[Path, Path]]]] = {"movie": {}, "tv": {},
                                                                                 "documentary": {},
                                                                                 "docuseries": {}}
    folder_map = {"movie": "Movies", "tv": "TV Shows", "documentary": "Documentaries",
                  "docuseries": "Docuseries"}

    for (data, _) in json_pair_list:
        mp4_path: Path = data.path.parent / "MP4"
        mp4_items: List[Mp4Item] = data.mp4_data_items
        corrections: List[Mp4Corrections] = data.mp4_data_corrections
        if not mp4_path.exists():
            error_list.append(f"File Organizer - Failed to find {mp4_path}")
            continue

        temp_corrections: List

        for mp4_item in mp4_items:
            file_list: List[Tuple[Path, Path]] = []
            output_folder_path = video_directory / folder_map[mp4_item.type.name] / mp4_item.folder_name
            if mp4_item.season != -1:
                output_folder_path = output_folder_path / "Season {:02}".format(mp4_item.season)

            if mp4_item.pattern == "":
                pattern = "*"
            else:
                pattern = "*" + mp4_item.pattern + "*"

            glob_list = list(mp4_path.glob(pattern))
            subtitle_path = mp4_path.parent / "SUB"
            subtitle_names = []
            if subtitle_path.exists():
                subtitle_paths = list(subtitle_path.glob(pattern))
                subtitle_names = [subtitle_path.name for subtitle_path in subtitle_paths]
                glob_list.extend(subtitle_paths)

            tv_mp4: list[str] = []
            mobile_mp4: list[str] = []
            for glob_item in glob_list:
                glob_name = str(glob_item.name)

                if glob_name in subtitle_names:
                    if glob_item.parent != subtitle_path:
                        continue

                in_exclusions: bool = False
                for exclusion in mp4_item.exclusions:
                    if exclusion in glob_name:
                        in_exclusions = True
                        break

                if in_exclusions:
                    continue

                for correction in corrections:
                    if correction.regex != "":
                        glob_name = re.sub(correction.regex, correction.replacement, glob_name)
                    else:
                        glob_name = glob_name.replace(correction.pattern, correction.replacement)

                if glob_name == "":
                    continue

                if "@tv.mp4" in glob_name:
                    tv_mp4.append(glob_name.replace("@tv.mp4", ""))
                if "@mobile.mp4" in glob_name:
                    mobile_mp4.append(glob_name.replace("@mobile.mp4", ""))

                if ".srt" in glob_name:
                    current_suffixes = Path(glob_name).suffixes
                    subtitles_is_valid, subtitles_string, subtitles_error = valid_suffixes(current_suffixes)
                    if not subtitles_is_valid:
                        if len(subtitles_error) > 0:
                            error_list.append(f"File Organizer - {glob_name} is invalid with error: {subtitles_error}")
                        continue

                    glob_name = glob_name.replace("".join(current_suffixes), subtitles_string)

                file_list.append((glob_item, output_folder_path / glob_name))

            if tv_mp4 or mobile_mp4:
                plex_versions_path = output_folder_path / "Plex Versions"
                plex_tv_path = plex_versions_path / "Optimized for TV"
                plex_mobile_path = plex_versions_path / "Optimized for Mobile"
                temp_list = []

                for (source_path, dest_path) in file_list:
                    dest_path_name = dest_path.name
                    stripped_name = dest_path_name
                    for suffix in dest_path.suffixes:
                        stripped_name = stripped_name.replace(suffix, "")
                    stripped_name = stripped_name.replace("@tv", "").replace("@mobile", "")
                    in_tv = stripped_name in tv_mp4
                    in_mobile = stripped_name in mobile_mp4
                    if not in_tv or not in_mobile:
                        continue

                    if "@tv.mp4" in dest_path_name:
                        temp_list.append((source_path, plex_tv_path / dest_path_name.replace("@tv.mp4", ".mp4")))
                    elif "@mobile.mp4" in dest_path_name:
                        temp_list.append((source_path,
                                          plex_mobile_path / dest_path_name.replace("@mobile.mp4", ".mp4")))

                    elif ".srt" in dest_path_name:
                        temp_list.append((source_path, dest_path))
                        if in_tv:
                            temp_list.append((source_path, plex_tv_path / dest_path_name))

                        if in_mobile:
                            temp_list.append((source_path, plex_mobile_path / dest_path_name))
                    else:
                        temp_list.append((source_path, dest_path))

                file_list = temp_list

            temp_list = []

            for (source_path, dest_path) in file_list:
                output_path = dest_path
                if str(output_path) in global_generated_list:
                    error_list.append(f"File Organizer - {source_path} has a duplicate output path {output_path}.")
                    continue
                temp_list.append((source_path, dest_path))
                global_generated_list.append(str(output_path))
            file_list = temp_list

            key = (mp4_item.folder_name, mp4_item.season)
            type_name = mp4_item.type.name
            if key in generated_list[type_name]:
                generated_list[type_name][key].extend(file_list)
            else:
                generated_list[type_name][key] = file_list

    def copy_function(backup_path: Path, video_path: Path):
        if video_path.exists():
            if video_path.stat().st_size == backup_path.stat().st_size:
                return
            else:
                video_path.unlink()
                time.sleep(0.05)

        if not video_path.parent.exists():
            video_path.parent.mkdir(parents=True, exist_ok=True)
        shutil.copy(backup_path, video_path)

    def copy_loop(type_str: str):
        print_with_delay(f"Copy {type_str} files across.")
        entry_list = tqdm(generated_list[type_str], position=0, leave=False, ncols=150)
        for entry in entry_list:
            plex_name, season = entry
            if season == -1:
                entry_list.set_description(plex_name)
            else:
                entry_list.set_description(f"{plex_name} Season {season}")
            for file_entry in generated_list[type_str][entry]:
                copy_function(file_entry[0], file_entry[1])

    def delete_loop(type_str: str):
        print_with_delay(f"Deleting extra {type_str} files.")
        current_dir: Path = video_directory / folder_map[type_str]
        file_glob = list(current_dir.rglob("*"))
        file_dir_list = []
        for file_item in file_glob:
            if file_item.is_dir():
                file_dir_list.append(file_item)
            else:
                if str(file_item) not in global_generated_list:
                    file_item.unlink()

        file_dir_list.sort(reverse=True)
        for folder_item in file_dir_list:
            try:
                folder_item.rmdir()
            except Exception:
                pass

    delete_loop("movie")
    copy_loop("movie")
    delete_loop("tv")
    copy_loop("tv")
    delete_loop("documentary")
    copy_loop("documentary")
    delete_loop("docuseries")
    copy_loop("docuseries")

    if len(error_list) > 0:
        print_with_delay("\r\n\r\nWe encountered the following errors.")
        for error in error_list:
            print(error)
