import os
import random
import string
import time
from pathlib import Path


def check_path(path: str):
    if os.path.exists(path):
        return Path(path)
    else:
        raise ValueError("Invalid path input.")


def print_with_delay(message: str):
    print(message)
    time.sleep(0.05)


def random_string_generator(str_size: int):
    return ''.join(random.SystemRandom().choice(string.ascii_letters + string.digits) for _ in range(str_size))
