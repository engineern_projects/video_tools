from typing import List
import time

from tqdm import tqdm

from .util import check_path, print_with_delay
from video_tools.info import utils
from video_tools.info.data import Data
from video_tools.info.status import Status


def generate_parser(sub_parser):
    parser = sub_parser.add_parser("update_status")
    parser.add_argument(
        "--scan_dir",
        type=check_path,
        help="Directory path to be scanned.",
        required=True
    )
    parser.set_defaults(func=main)


def main(args):
    print_with_delay("Scanning for data.json and status.json files.")
    json_pair_list, json_err = utils.get_info_pairs(args.scan_dir)
    if json_err:
        raise Exception(json_err)

    error_list: List[str] = utils.check_pairs_for_errors(json_pair_list)

    print_with_delay("Updating status.json files.")
    check_progress = tqdm(json_pair_list, ncols=150)
    for pair in check_progress:
        _data: Data = pair[0]
        _status: Status = pair[1]
        _status.save()
    check_progress.close()

    if len(error_list) > 0:
        print_with_delay("\r\n\r\nWe encountered the following errors.")
        time.sleep(0.05)
        for error in error_list:
            print(error)
