import os
import shutil
import tempfile
from dataclasses import dataclass
from pathlib import Path
from typing import List, Tuple

from tqdm import tqdm

from video_tools.tools import hash
from .util import check_path, print_with_delay
from video_tools.info import utils
from video_tools.info.data import Data
from video_tools.info.status import Status


def generate_parser(sub_parser):
    parser = sub_parser.add_parser("check_iso_hash", description="Check the md5 hash of the ISO files.")
    parser.add_argument(
        "--scan_dir",
        type=check_path,
        help="Directory path to be scanned.",
        required=True
    )
    parser.add_argument(
        "--temp_dir",
        type=check_path,
        help="Directory path to be used for temporary data.",
        required=True
    )
    parser.set_defaults(func=main)


def main(args):
    print_with_delay("Scanning for data.json and status.json files.")
    json_pair_list, json_err = utils.get_info_pairs(args.scan_dir)
    if json_err:
        raise Exception(json_err)

    error_list: List[str] = utils.check_pairs_for_errors(json_pair_list)

    print_with_delay("Checking for files that need hash checked.")

    @dataclass
    class IsoDataList:
        data: Data
        status: Status
        file_list: List[Tuple[Path, str]]

    iso_list: List[IsoDataList] = list()

    check_progress = tqdm(json_pair_list, ncols=150)
    for pair in check_progress:
        _data: Data = pair[0]
        _status: Status = pair[1]
        check_progress.set_description(_data.name)
        check_list = utils.get_iso_check_list(_data, _status)
        if len(check_list) > 0:
            iso_list.append(IsoDataList(_data, _status, check_list))
        check_progress.set_description()
    check_progress.close()

    if len(iso_list) > 0:
        print_with_delay("Hashing files that need to be checked.")
        iso_progress = tqdm(iso_list, position=0, leave=None, ncols=150)
        ctrl_c = False
        for iso in iso_progress:
            if ctrl_c:
                break
            _data: Data = iso.data
            _status: Status = iso.status
            _file_list: List[Path] = iso.file_list
            for file_data in _file_list:
                _file: Path = file_data
                name = _file.name
                temp_dir = tempfile.TemporaryDirectory(dir=args.temp_dir)

                try:
                    iso_progress.set_description(name + " - Copying to Temp")
                    shutil.copy2(_file, temp_dir.name)

                    iso_progress.set_description(name + " - Checking md5 Hash")
                    temp_file = Path(temp_dir.name + "/" + _file.name)
                    hash_ref = hash.md5(temp_file)
                except KeyboardInterrupt:
                    os.remove(temp_dir.name + "/" + _file.name)
                    temp_dir.cleanup()
                    ctrl_c = True
                    break
                except Exception as e:
                    error_list.append("Check error: Failed to process {0} with error - {1}".format(_file, e))
                    continue

                iso_progress.set_description(name + " - Cleanup Temp")
                os.remove(temp_dir.name + "/" + _file.name)
                temp_dir.cleanup()

                iso_progress.set_description(name + " - Updating status")
                update_error = _status.update_iso_check_data(_file.name, hash_ref)
                if update_error:
                    error_list.append(update_error)
                _status.save()
                iso_progress.set_description()
        iso_progress.close()
    else:
        print("No files need to be checked.")

    if len(error_list) > 0:
        print_with_delay("\r\n\r\nWe encountered the following errors.")
        for error in error_list:
            print(error)
