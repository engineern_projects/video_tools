import os
import shutil
import tempfile
import time
from dataclasses import dataclass
from pathlib import Path
from typing import List, Tuple

from tqdm import tqdm

from video_tools.info import utils
from video_tools.info.data import Data, SplitData
from video_tools.info.status import Status, SplitStatus
from video_tools.tools import mkvmerge
from .util import check_path, print_with_delay, random_string_generator


def generate_parser(sub_parser):
    parser = sub_parser.add_parser("mkv_split", description="")
    parser.add_argument(
        "--scan_dir",
        type=check_path,
        help="Directory path to be scanned.",
        required=True
    )
    parser.add_argument(
        "--temp_dir",
        type=check_path,
        help="Directory path to be used for temporary data.",
        required=True
    )
    parser.set_defaults(func=main)


def main(args):
    print_with_delay("Scanning for data.json and status.json files.")
    json_pair_list, json_err = utils.get_info_pairs(args.scan_dir)
    if json_err:
        raise Exception(json_err)

    error_list: List[str] = utils.check_pairs_for_errors(json_pair_list)

    print_with_delay("Checking for files that need to be split.")

    @dataclass
    class SplitInfo:
        data: Data
        status: Status
        file_list: List[Tuple[SplitData, SplitStatus]]

    merge_list: List[SplitInfo] = list()

    check_progress = tqdm(json_pair_list, ncols=150)
    for pair in check_progress:
        _data: Data = pair[0]
        _status: Status = pair[1]
        check_progress.set_description(_data.name)
        check_list = utils.get_mkv_split_list(_data, _status)
        if len(check_list) > 0:
            merge_list.append(SplitInfo(_data, _status, check_list))
        check_progress.set_description()

    if len(merge_list) > 0:
        print("Splitting files that need to be split.")
        time.sleep(0.05)
        split_progress = tqdm(merge_list, position=0, leave=None, ncols=150)
        ctrl_c = False
        for split_info in split_progress:
            if ctrl_c:
                break
            _data: Data = split_info.data
            _status: Status = split_info.status
            _file_list: List[Tuple[SplitData, SplitStatus]] = split_info.file_list
            for split_data, merge_status in _file_list:
                name = split_data.filename
                temp_dir = tempfile.TemporaryDirectory(dir=args.temp_dir)
                temp_dir_path = Path(temp_dir.name)
                mkv_dir = _data.path.parent / "MKV"
                source_file = mkv_dir / name
                temp_file = temp_dir_path / name

                try:
                    split_progress.set_description(name + " - Copying to Temp")
                    shutil.copy2(source_file, temp_dir_path)
                    new_temp_file = temp_dir_path / (random_string_generator(10) + name)
                    old_temp_file_path = str(temp_file)
                    temp_file.rename(new_temp_file)
                    temp_file = new_temp_file

                    split_progress.set_description(name + " - Creating split mkv")
                    split_paths, split_error = mkvmerge.split(temp_file, split_data.chapters, old_temp_file_path)
                    if not split_paths:
                        raise Exception(split_error)

                    split_progress.set_description(name + " - Copy MKV files")
                    for split_path in split_paths:
                        shutil.copy2(split_path, mkv_dir)
                except KeyboardInterrupt:
                    for filename in os.listdir(temp_dir.name):
                        os.remove(temp_dir.name + "/" + filename)
                    temp_dir.cleanup()
                    ctrl_c = True
                    break
                except Exception as e:
                    for filename in os.listdir(temp_dir.name):
                        os.remove(temp_dir.name + "/" + filename)
                    temp_dir.cleanup()
                    error_list.append("Check error: Failed to process {0} with error - {1}".format(name, e))
                    continue

                split_progress.set_description(name + " - Cleanup Temp")
                for filename in os.listdir(temp_dir.name):
                    os.remove(temp_dir.name + "/" + filename)
                temp_dir.cleanup()

                split_progress.set_description(name + " - Updating status")
                update_error = _status.update_mkv_split(name)
                if update_error:
                    error_list.append(update_error)
                _status.save()
                split_progress.set_description()
    else:
        print("No files need to be checked.")

    if len(error_list) > 0:
        print("\r\n\r\nWe encountered the following errors.")
        for error in error_list:
            print(error)
