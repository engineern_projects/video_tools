import os
from dataclasses import dataclass
from pathlib import Path
from typing import List

from tqdm import tqdm

from .util import check_path, print_with_delay
from video_tools.info import utils
from video_tools.info.data import Data
from video_tools.info.status import Status


def generate_parser(sub_parser):
    parser = sub_parser.add_parser("mkv_cleanup")
    parser.add_argument(
        "--scan_dir",
        type=check_path,
        help="Directory path to be scanned.",
        required=True
    )
    parser.set_defaults(func=main)


def main(args):
    print_with_delay("Scanning for data.json and status.json files.")
    json_pair_list, json_err = utils.get_info_pairs(args.scan_dir)
    if json_err:
        raise Exception(json_err)

    error_list: List[str] = utils.check_pairs_for_errors(json_pair_list)

    print_with_delay("Checking for MKV files that need to be removed.")

    @dataclass
    class MkvDataList:
        data: Data
        status: Status
        file_list: List[Path]

    mkv_list: List[MkvDataList] = list()

    check_progress = tqdm(json_pair_list, ncols=150)
    for pair in check_progress:
        _data: Data = pair[0]
        _status: Status = pair[1]
        check_progress.set_description(_data.name)
        delete_list_error, delete_list = utils.get_delete_list(_data, _status)
        if delete_list_error == "" and len(delete_list) > 0:
            mkv_list.append(MkvDataList(_data, _status, delete_list))
        elif delete_list_error != "":
            error_list.append(delete_list_error)
        check_progress.set_description()
    check_progress.close()

    if len(mkv_list) > 0:
        print_with_delay("Deleting MKV files.")
        mkv_progress = tqdm(mkv_list, position=0, leave=None, ncols=150)
        ctrl_c = False
        for mkv in mkv_progress:
            if ctrl_c:
                break
            _data: Data = mkv.data
            _status: Status = mkv.status
            _file_list: List[Path] = mkv.file_list

            mkv_progress.set_description(_data.name + " - Deleting MKV files")

            for file_path in _file_list:
                if file_path.exists():
                    os.remove(file_path)

            mkv_progress.set_description()
        mkv_progress.close()
    else:
        print("No files need to be deleted.")

    if len(error_list) > 0:
        print_with_delay("\r\n\r\nWe encountered the following errors.")
        for error in error_list:
            print(error)
