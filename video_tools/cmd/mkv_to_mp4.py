import os
import shutil
import tempfile
from dataclasses import dataclass
from pathlib import Path
from typing import List, Tuple

from tqdm import tqdm

from video_tools.info.common import Resolution
from video_tools.tools import handbrake, subtitle_edit
from .util import check_path, print_with_delay
from video_tools.info import utils
from video_tools.info.data import Data
from video_tools.info.status import Status


def generate_parser(sub_parser):
    parser = sub_parser.add_parser("mkv_to_mp4", description="Convert MKV files to MP4.")
    parser.add_argument(
        "--scan_dir",
        type=check_path,
        help="Directory path to be scanned.",
        required=True
    )
    parser.add_argument(
        "--temp_dir",
        type=check_path,
        help="Directory path to be used for temporary data.",
        required=True
    )
    parser.set_defaults(func=main)


def main(args):
    print_with_delay("Scanning for data.json and status.json files.")
    json_pair_list, json_err = utils.get_info_pairs(args.scan_dir)
    if json_err:
        raise Exception(json_err)

    error_list: List[str] = utils.check_pairs_for_errors(json_pair_list)

    print_with_delay("Checking for MKV files that need converted to MP4.")

    @dataclass
    class MkvDataList:
        data: Data
        status: Status
        file_list: List[Path]

    mkv_list: List[MkvDataList] = list()

    check_progress = tqdm(json_pair_list, ncols=150)
    for pair in check_progress:
        _data: Data = pair[0]
        _status: Status = pair[1]
        check_progress.set_description(_data.name)
        check_list = utils.get_mkv_mp4_convert_list(_data, _status)
        if len(check_list) > 0:
            mkv_list.append(MkvDataList(_data, _status, check_list))
        check_progress.set_description()
    check_progress.close()

    if len(mkv_list) > 0:
        print_with_delay("Converting MKV files to MP4 files.")
        mkv_progress = tqdm(mkv_list, position=0, leave=None, ncols=150)
        ctrl_c = False
        for mkv in mkv_progress:
            if ctrl_c:
                break
            _data: Data = mkv.data
            _status: Status = mkv.status
            _file_list: List[Path] = mkv.file_list
            for file_data in _file_list:
                _file: Path = file_data
                name = _file.name
                parent_name = _file.parent.name
                find_name = name
                destination_folder = _file.parent / ".." / "MP4"
                if parent_name != "MKV":
                    find_name = parent_name + "/" + name
                    destination_folder = _file.parent / ".." / ".." / "MP4"

                temp_dir = tempfile.TemporaryDirectory(dir=args.temp_dir)

                try:
                    mkv_progress.set_description(name + " - Gathering Data")
                    resolution, resolution_error = _data.get_mkv_resolution(find_name)
                    if resolution_error:
                        raise Exception(resolution_error)

                    plex_name, plex_name_error = _data.get_mkv_plex_name(find_name)
                    if plex_name_error:
                        raise Exception(plex_name_error)

                    mkv_progress.set_description(name + " - Copying to Temp")
                    temp_file = Path(temp_dir.name + "/" + plex_name + ".mkv")
                    shutil.copy2(_file, temp_file)

                    mkv_progress.set_description(name + " - Create Original Quality MP4")
                    check, check_error = _status.get_mkv_converted_original(find_name)
                    if check_error:
                        raise Exception(check_error)

                    if not check:
                        mp4_path, mp4_error = handbrake.generate_file(temp_file, plex_name, resolution, resolution)
                        if not mp4_path:
                            raise Exception(mp4_error)

                        if not destination_folder.exists():
                            os.mkdir(destination_folder)
                        destination_path = destination_folder / mp4_path.name
                        if destination_path.exists():
                            os.remove(destination_path)
                        shutil.copy2(mp4_path, destination_path)
                        os.remove(mp4_path)

                        mkv_progress.set_description(name + " - Updating status")
                        update_error = _status.mark_mkv_converted_original(find_name)
                        if update_error:
                            raise Exception(update_error)
                        _status.save()

                    mkv_progress.set_description(name + " - Create TV Quality MP4")
                    check, check_error = _status.get_mkv_converted_tv(find_name)
                    if check_error:
                        raise Exception(check_error)

                    if not check:
                        if resolution.value == Resolution.r2160p.value:
                            mp4_path, mp4_error = handbrake.generate_file(temp_file, plex_name, resolution,
                                                                          Resolution.tv)
                            if not mp4_path:
                                raise Exception(mp4_error)

                            if not destination_folder.exists():
                                os.mkdir(destination_folder)
                            destination_path = destination_folder / mp4_path.name
                            if destination_path.exists():
                                os.remove(destination_path)
                            shutil.copy2(mp4_path, destination_path)
                            os.remove(mp4_path)

                        mkv_progress.set_description(name + " - Updating status")
                        update_error = _status.mark_mkv_converted_tv(find_name)
                        if update_error:
                            raise Exception(update_error)
                        _status.save()

                    mkv_progress.set_description(name + " - Create Mobile Quality MP4")
                    check, check_error = _status.get_mkv_converted_mobile(find_name)
                    if check_error:
                        raise Exception(check_error)

                    if not check:
                        if resolution.value == Resolution.r2160p.value:
                            mp4_path, mp4_error = handbrake.generate_file(temp_file, plex_name, resolution,
                                                                          Resolution.mobile)
                            if not mp4_path:
                                raise Exception(mp4_error)

                            if not destination_folder.exists():
                                os.mkdir(destination_folder)
                            destination_path = destination_folder / mp4_path.name
                            if destination_path.exists():
                                os.remove(destination_path)
                            shutil.copy2(mp4_path, destination_path)
                            os.remove(mp4_path)

                        mkv_progress.set_description(name + " - Updating status")
                        update_error = _status.mark_mkv_converted_mobile(find_name)
                        if update_error:
                            raise Exception(update_error)
                        _status.save()

                    mkv_progress.set_description(name + " - Extract Subtitles")
                    check, check_error = _status.get_mkv_extracted_subtitles(find_name)
                    if check_error:
                        raise Exception(check_error)

                    if not check:
                        subtitle_paths, subtitle_error = subtitle_edit.generate_subtitles(temp_file)
                        if subtitle_paths is None:
                            raise Exception(subtitle_error)
                        for subtitle_path in subtitle_paths:
                            if not destination_folder.exists():
                                os.mkdir(destination_folder)
                            destination_path = destination_folder / subtitle_path.name
                            if destination_path.exists():
                                os.remove(destination_path)
                            shutil.copy2(subtitle_path, destination_path)
                            os.remove(subtitle_path)

                        mkv_progress.set_description(name + " - Updating status")
                        update_error = _status.mark_mkv_extracted_subtitles(find_name)
                        if update_error:
                            raise Exception(update_error)
                        _status.save()

                except KeyboardInterrupt:
                    for filename in os.listdir(temp_dir.name):
                        os.remove(temp_dir.name + "/" + filename)
                    temp_dir.cleanup()
                    ctrl_c = True
                    break
                except Exception as e:
                    for filename in os.listdir(temp_dir.name):
                        os.remove(temp_dir.name + "/" + filename)
                    temp_dir.cleanup()
                    error_list.append("MKV error: Failed to process {0} with error - {1}".format(_file, e))
                    continue

                mkv_progress.set_description(name + " - Cleanup Temp")
                for filename in os.listdir(temp_dir.name):
                    os.remove(temp_dir.name + "/" + filename)
                temp_dir.cleanup()

                mkv_progress.set_description()
        mkv_progress.close()
    else:
        print("No files need to be extracted.")

    if len(error_list) > 0:
        print_with_delay("\r\n\r\nWe encountered the following errors.")
        for error in error_list:
            print(error)
