import os
import shutil
import tempfile
from dataclasses import dataclass
from pathlib import Path
from typing import List, Tuple

from tqdm import tqdm

from video_tools.tools import makemkv
from .util import check_path, print_with_delay
from video_tools.info import utils
from video_tools.info.data import Data
from video_tools.info.status import Status


def generate_parser(sub_parser):
    parser = sub_parser.add_parser("iso_to_mkv")
    parser.add_argument(
        "--scan_dir",
        type=check_path,
        help="Directory path to be scanned.",
        required=True
    )
    parser.add_argument(
        "--temp_dir",
        type=check_path,
        help="Directory path to be used for temporary data.",
        required=True
    )
    parser.set_defaults(func=main)


def main(args):
    print_with_delay("Scanning for data.json and status.json files.")
    json_pair_list, json_err = utils.get_info_pairs(args.scan_dir)
    if json_err:
        raise Exception(json_err)

    error_list: List[str] = utils.check_pairs_for_errors(json_pair_list)

    print_with_delay("Checking for ISO files that need MKV extracted.")

    @dataclass
    class IsoDataList:
        data: Data
        status: Status
        file_list: List[Path]

    iso_list: List[IsoDataList] = list()

    check_progress = tqdm(json_pair_list, ncols=150)
    for pair in check_progress:
        _data: Data = pair[0]
        _status: Status = pair[1]
        check_progress.set_description(_data.name)
        check_list = utils.get_iso_mkv_extract_list(_data, _status)
        if len(check_list) > 0:
            iso_list.append(IsoDataList(_data, _status, check_list))
        check_progress.set_description()
    check_progress.close()

    if len(iso_list) > 0:
        print_with_delay("Extracting MKV files from ISO files.")
        iso_progress = tqdm(iso_list, position=0, leave=None, ncols=150)
        ctrl_c = False
        for iso in iso_progress:
            if ctrl_c:
                break
            _data: Data = iso.data
            _status: Status = iso.status
            _file_list: List[Path] = iso.file_list
            for file_data in _file_list:
                _file: Path = file_data
                name = _file.name
                temp_dir = tempfile.TemporaryDirectory(dir=args.temp_dir)

                try:
                    custom_folder, custom_folder_error = _data.get_iso_custom_mkv_folder(name)
                    if custom_folder_error:
                        raise Exception(custom_folder_error)

                    iso_progress.set_description(name + " - Copying to Temp")
                    shutil.copy2(_file, temp_dir.name)

                    iso_progress.set_description(name + " - Checking MKV titles")
                    temp_file = Path(temp_dir.name + "/" + _file.name)
                    title_count, title_error = makemkv.get_title_count(temp_file)
                    if title_error:
                        raise Exception(title_error)

                    mkv_count, mkv_count_error = _status.get_iso_mkv_count(name)
                    if mkv_count_error:
                        raise Exception(mkv_count_error)

                    mkv_missing_list, mkv_list = _data.get_mkv_list_from_iso_filename(name)
                    if len(mkv_list) == 0:
                        title_list = range(mkv_count, title_count)
                    elif len(mkv_missing_list) == 0:
                        title_list = []
                    else:
                        title_list = []
                        for mkv_item in mkv_missing_list:
                            mkv_item_index, mkv_item_error = _data.get_mkv_index(mkv_item, name)
                            if mkv_item_index != -1:
                                title_list.append(mkv_item_index)

                    for title_id in title_list:
                        iso_progress.set_description(name + " MKV #" + str(title_id) + " - Generating file")
                        mkv_path, mkv_error = makemkv.generate_title(temp_file, title_id)
                        if mkv_error:
                            raise Exception(mkv_error)

                        iso_progress.set_description(name + " MKV #" + str(title_id) + " - Copy MKV files")
                        if custom_folder is None or custom_folder == "":
                            destination_folder = _file.parent / ".." / "MKV"
                        else:
                            destination_folder = _file.parent / ".." / "MKV" / custom_folder
                        if not destination_folder.exists():
                            destination_folder.mkdir(parents=True, exist_ok=True)
                        destination_path = destination_folder / mkv_path.name
                        if destination_path.exists():
                            os.remove(destination_path)
                        shutil.copy2(mkv_path, destination_path)
                        os.remove(mkv_path)

                        iso_progress.set_description(name + " MKV #" + str(title_id) + " - Updating status")
                        update_error = _status.update_iso_mkv_count(_file.name, title_id + 1)
                        if update_error:
                            error_list.append(update_error)
                        _status.save()
                except KeyboardInterrupt:
                    for filename in os.listdir(temp_dir.name):
                        os.remove(temp_dir.name + "/" + filename)
                    temp_dir.cleanup()
                    ctrl_c = True
                    break
                except Exception as e:
                    for filename in os.listdir(temp_dir.name):
                        os.remove(temp_dir.name + "/" + filename)
                    temp_dir.cleanup()
                    error_list.append("MKV error: Failed to process {0} with error - {1}".format(_file, e))
                    continue

                iso_progress.set_description(name + " - Cleanup Temp")
                for filename in os.listdir(temp_dir.name):
                    os.remove(temp_dir.name + "/" + filename)
                temp_dir.cleanup()

                iso_progress.set_description(name + " - Updating status")
                update_error = _status.mark_iso_mkv_extracted(_file.name)
                if update_error:
                    error_list.append(update_error)
                _status.save()
                iso_progress.set_description()
        iso_progress.close()
    else:
        print("No files need to be extracted.")

    if len(error_list) > 0:
        print_with_delay("\r\n\r\nWe encountered the following errors.")
        for error in error_list:
            print(error)
