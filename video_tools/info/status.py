import json
import os.path
from dataclasses import dataclass
from datetime import date, datetime
from typing import List, Optional, Tuple

from .data import Data


@dataclass
class IsoStatus:
    filename: str = ""
    hash: str = ""
    last_checked: date = datetime(2000, 1, 1)
    mkv_extracted: bool = False
    mkv_count: int = 0


@dataclass
class MkvStatus:
    filename: str = ""
    converted_original: bool = False
    converted_tv: bool = False
    converted_mobile: bool = False
    extracted_subtitles: bool = False


@dataclass
class MergeStatus:
    filename: str = ""
    merged: bool = False


@dataclass
class SplitStatus:
    filename: str = ""
    split: bool = False


class Status:
    def __init__(self, data: Data):
        self.iso_status: List[IsoStatus] = []
        self.merge_status: List[MergeStatus] = []
        self.split_status: List[SplitStatus] = []
        self.mkv_status: List[MkvStatus] = []

        self.name: str = ""
        self.is_valid: bool = False
        self.parse_errors: List[str] = []
        self.last_edited: date = datetime(2000, 1, 1)

        self.__load_data(data)

    def __load_data(self, data: Data):
        self.path = data.path.parent / "status.json"
        self.name = data.name

        if self.path.exists():
            # noinspection PyBroadException
            try:
                with open(self.path, encoding='utf-8') as json_file:
                    json_data = json.load(json_file)
            except Exception as _e:
                self.parse_errors.append("failed to parse json file.")
                return

            self.last_edited = datetime.fromtimestamp(self.path.lstat().st_mtime)

            if "__DO_NOT_TOUCH__" not in json_data.keys():
                self.parse_errors.append("failed to find a __DO_NOT_TOUCH__ in json.")
                return
            elif not isinstance(json_data["__DO_NOT_TOUCH__"], str):
                self.parse_errors.append("__DO_NOT_TOUCH__ is an invalid type.")
                return

            if "iso" in json_data.keys():
                if not isinstance(json_data["iso"], list):
                    self.parse_errors.append("iso is an invalid type.")
                else:
                    mkv_folder_cache_success = []
                    mkv_folder_cache_failed = []

                    for count, item in enumerate(json_data["iso"]):
                        if "filename" not in item.keys():
                            self.parse_errors.append("failed to find filename in iso #{0}.".format(count + 1))
                            continue
                        elif not isinstance(item["filename"], str) or ".iso" not in item["filename"] or not (
                                self.path.parent / "Backup" / item["filename"]).exists():
                            self.parse_errors.append("filename in iso #{0} is invalid.".format(count + 1))

                            if not isinstance(item["filename"], str):
                                continue

                        if "last_checked" not in item.keys():
                            self.parse_errors.append("failed to find last_checked in iso #{0}.".format(count + 1))
                            item["last_checked"] = "1-1-2000 00:00:00"
                        elif not isinstance(item["last_checked"], str):
                            self.parse_errors.append("last_checked in iso #{0} is invalid.".format(count + 1))
                            item["last_checked"] = "1-1-2000 00:00:00"

                        if "mkv_extracted" not in item.keys():
                            self.parse_errors.append("failed to find mkv_extracted in iso #{0}.".format(count + 1))
                            item["mkv_extracted"] = False
                        elif not isinstance(item["mkv_extracted"], bool):
                            self.parse_errors.append("mkv_extracted in iso #{0} is invalid.".format(count + 1))
                            item["mkv_extracted"] = False

                        if "mkv_count" not in item.keys():
                            self.parse_errors.append("failed to find mkv_count in iso #{0}.".format(count + 1))
                            item["mkv_count"] = 0
                        elif not isinstance(item["mkv_count"], int):
                            self.parse_errors.append("mkv_count in iso #{0} is invalid.".format(count + 1))
                            item["mkv_count"] = 0

                        hash_path = self.path.parent / "Backup" / item["filename"].replace(".iso", ".md5")
                        if hash_path.exists():
                            with open(hash_path) as f:
                                hash_str = f.read()

                            try:
                                last_checked = datetime.strptime(item["last_checked"], '%m-%d-%Y %H:%M:%S')
                            except ValueError as e:
                                self.parse_errors.append(
                                    "failed to parse last_checked in iso #{0} with error - {1}".format(count + 1, e))
                                continue

                            case_filename, _ = data.get_original_iso_filename(item["filename"])
                            if data.parse_errors is None and case_filename == "":
                                self.parse_errors.append(
                                    "failed to find iso #{0}, removing from list.".format(count + 1))
                                continue
                            elif case_filename != "":
                                item["filename"] = case_filename

                            filename = item["filename"]
                            custom, _ = data.get_iso_custom_mkv_folder(filename)
                            mkv_path = self.path.parent / "MKV" / custom

                            if mkv_path in mkv_folder_cache_success:
                                pass
                            elif mkv_path in mkv_folder_cache_failed:
                                item["mkv_extracted"] = False
                                item["mkv_count"] = 0
                            elif not (self.path.parent / "MKV").exists() or \
                                    len(list(mkv_path.glob("*.mkv"))) == 0:
                                item["mkv_extracted"] = False
                                item["mkv_count"] = 0
                                mkv_folder_cache_failed.append(mkv_path)
                            else:
                                mkv_folder_cache_success.append(mkv_path)

                            if item["mkv_extracted"]:
                                mkv_missing_list, mkv_list = data.get_mkv_list_from_iso_filename(filename)

                                if len(mkv_missing_list) != 0:
                                    item["mkv_extracted"] = False
                                    item["mkv_count"] = 0

                            self.iso_status.append(IsoStatus(item["filename"], hash_str, last_checked,
                                                             item["mkv_extracted"], item["mkv_count"]))
                        else:
                            self.parse_errors.append("failed to find hash for iso #{0}.".format(count + 1))

            if "merge" in json_data.keys():
                if not isinstance(json_data["merge"], list):
                    self.parse_errors.append("merge is an invalid type.")
                else:
                    for count, item in enumerate(json_data["merge"]):
                        if "filename" not in item.keys():
                            self.parse_errors.append("failed to find filename in merge #{0}.".format(count + 1))
                            continue
                        elif not isinstance(item["filename"], str) or ".mkv" not in item["filename"] or not (
                                self.path.parent / "MKV" / item["filename"]).exists():
                            self.parse_errors.append("filename in merge #{0} is invalid.".format(count + 1))
                            item["merged"] = False
                            if not isinstance(item["filename"], str):
                                continue

                        if "merged" not in item.keys():
                            self.parse_errors.append("failed to find merged in merge #{0}.".format(count + 1))
                            item["merged"] = False
                        elif not isinstance(item["merged"], bool):
                            self.parse_errors.append("merged in merge #{0} is invalid.".format(count + 1))
                            item["merged"] = False

                        case_filename, _ = data.get_original_mkv_merge_filename(item["filename"])
                        if data.parse_errors is None and case_filename == "":
                            self.parse_errors.append("failed to find merge #{0}, removing from list.".format(count + 1))
                            continue
                        elif case_filename != "":
                            item["filename"] = case_filename

                        self.merge_status.append(MergeStatus(item["filename"], item["merged"]))

            if "split" in json_data.keys():
                if not isinstance(json_data["split"], list):
                    self.parse_errors.append("split is an invalid type.")
                else:
                    for count, item in enumerate(json_data["split"]):
                        if "filename" not in item.keys():
                            self.parse_errors.append("failed to find filename in split #{0}.".format(count + 1))
                            continue
                        elif not isinstance(item["filename"], str) or ".mkv" not in item["filename"]:
                            self.parse_errors.append("filename in split #{0} is invalid.".format(count + 1))

                            if not isinstance(item["filename"], str):
                                continue
                        elif not (self.path.parent / "MKV" / item["filename"]).exists():
                            self.parse_errors.append("filename in split #{0} does not exist.".format(count + 1))

                        if "split" not in item.keys():
                            self.parse_errors.append("failed to find split in split #{0}.".format(count + 1))
                            item["split"] = False
                        elif not isinstance(item["split"], bool):
                            self.parse_errors.append("split in split #{0} is invalid.".format(count + 1))
                            item["split"] = False

                        case_filename, _ = data.get_original_mkv_split_filename(item["filename"])
                        if data.parse_errors is None and case_filename == "":
                            self.parse_errors.append("failed to find split #{0}, removing from list.".format(count + 1))
                            continue
                        elif case_filename != "":
                            item["filename"] = case_filename

                        mkv_path = self.path.parent / "MKV"
                        if not mkv_path.exists() or \
                                len(list(mkv_path.glob(os.path.splitext(case_filename)[0] + "-0*.mkv"))) == 0:
                            item["split"] = False

                        self.split_status.append(SplitStatus(item["filename"], item["split"]))

            if "mkv" in json_data.keys():
                if not isinstance(json_data["mkv"], list):
                    self.parse_errors.append("mkv is an invalid type.")
                else:
                    for count, item in enumerate(json_data["mkv"]):
                        if "filename" not in item.keys():
                            self.parse_errors.append("failed to find filename in mkv #{0}.".format(count + 1))
                            continue
                        elif not isinstance(item["filename"], str) or ".mkv" not in item["filename"] or not (
                                self.path.parent / "MKV" / item["filename"]).exists():
                            self.parse_errors.append("filename in mkv #{0} is invalid.".format(count + 1))

                            if not isinstance(item["filename"], str):
                                continue

                        if "converted_original" not in item.keys():
                            self.parse_errors.append("failed to find converted_original in mkv #{0}.".format(count + 1))
                            item["converted_original"] = False
                        elif not isinstance(item["converted_original"], bool):
                            self.parse_errors.append("converted_original in mkv #{0} is invalid.".format(count + 1))
                            item["converted_original"] = False

                        if "converted_tv" not in item.keys():
                            self.parse_errors.append("failed to find converted_tv in mkv #{0}.".format(count + 1))
                            item["converted_tv"] = False
                        elif not isinstance(item["converted_tv"], bool):
                            self.parse_errors.append("converted_tv in mkv #{0} is invalid.".format(count + 1))
                            item["converted_tv"] = False

                        if "converted_mobile" not in item.keys():
                            self.parse_errors.append("failed to find converted_mobile in mkv #{0}.".format(count + 1))
                            item["converted_mobile"] = False
                        elif not isinstance(item["converted_mobile"], bool):
                            self.parse_errors.append("converted_mobile in mkv #{0} is invalid.".format(count + 1))
                            item["converted_mobile"] = False

                        if "extracted_subtitles" not in item.keys():
                            self.parse_errors.append(
                                "failed to find extracted_subtitles in mkv #{0}.".format(count + 1))
                            item["extracted_subtitles"] = False
                        elif not isinstance(item["extracted_subtitles"], bool):
                            self.parse_errors.append("extracted_subtitles in mkv #{0} is invalid.".format(count + 1))
                            item["extracted_subtitles"] = False

                        name, _ = data.get_mkv_plex_name(item["filename"])

                        if len(data.parse_errors) == 0 and name == "":
                            self.parse_errors.append("failed to find mkv #{0}, removing from list.".format(count + 1))
                            continue

                        case_filename, _ = data.get_original_mkv_filename(item["filename"])
                        if data.parse_errors is None and case_filename == "":
                            self.parse_errors.append("failed to find mkv #{0}, removing from list.".format(count + 1))
                            continue
                        elif case_filename != "":
                            item["filename"] = case_filename

                        mp4_path = self.path.parent / "MP4"
                        if not mp4_path.exists():
                            item["converted_original"] = False
                            item["converted_tv"] = False
                            item["converted_mobile"] = False
                            item["extracted_subtitles"] = False
                        else:
                            # Don't separately look for subtitles. They may not exist on mkv.
                            file_list = list(mp4_path.glob(name + "*"))
                            if name != "" and len(file_list) == 0:
                                item["converted_original"] = False
                                item["converted_tv"] = False
                                item["converted_mobile"] = False
                                item["extracted_subtitles"] = False

                        self.mkv_status.append(MkvStatus(item["filename"],
                                                         item["converted_original"],
                                                         item["converted_tv"],
                                                         item["converted_mobile"],
                                                         item["extracted_subtitles"]))

        self.is_valid = True
        for count, iso in enumerate(data.iso_data):
            iso_found: bool = False

            for iso_status in self.iso_status:
                if iso.filename.lower() == iso_status.filename.lower():
                    iso_found = True
                    break

            if not iso_found:
                iso_status = IsoStatus()
                iso_status.filename = iso.filename
                hash_path = data.path.parent / "Backup" / iso.filename.replace(".iso", ".md5")
                if hash_path.exists():
                    with open(hash_path) as f:
                        iso_status.hash = f.read()
                    self.iso_status.append(iso_status)
                else:
                    self.parse_errors.append("failed to find hash for iso #{0}.".format(count + 1))

        for count, split in enumerate(data.split_data):
            split_found: bool = False

            for mkv_status in self.split_status:
                if split.filename.lower() == mkv_status.filename.lower():
                    split_found = True
                    break

            if not split_found:
                split_status = SplitStatus()
                split_status.filename = split.filename
                self.split_status.append(split_status)

        for count, merge in enumerate(data.merge_data):
            merge_found: bool = False

            for merge_status in self.merge_status:
                if merge.output_name.lower() == merge_status.filename.lower():
                    merge_found = True
                    break

            if not merge_found:
                merge_status = MergeStatus()
                merge_status.filename = merge.output_name
                self.merge_status.append(merge_status)

        for count, mkv in enumerate(data.mkv_data):
            mkv_found: bool = False

            for mkv_status in self.mkv_status:
                if mkv.filename.lower() == mkv_status.filename.lower():
                    mkv_found = True
                    break

            if not mkv_found:
                mkv_status = MkvStatus()
                mkv_status.filename = mkv.filename
                self.mkv_status.append(mkv_status)

        self.iso_status = self.remove_duplicate(self.iso_status)
        self.split_status = self.remove_duplicate(self.split_status)
        self.merge_status = self.remove_duplicate(self.merge_status)
        self.mkv_status = self.remove_duplicate(self.mkv_status)

    @staticmethod
    def remove_duplicate(data: List[any]) -> List[any]:
        unique_items = []

        for item in data:
            if item not in unique_items:
                unique_items.append(item)

        return unique_items

    def update_iso_check_data(self, filename: str, hash_str: str) -> Optional[str]:
        for status in self.iso_status:
            if status.filename == filename:
                if status.hash == hash_str:
                    status.last_checked = datetime.now()
                    return None
                else:
                    return self.name + " had a mismatched hash."
        return self.name + " could not find " + filename

    def get_iso_mkv_count(self, filename: str) -> Tuple[int, Optional[str]]:
        for status in self.iso_status:
            if status.filename == filename:
                return status.mkv_count, None
        return False, self.name + " could not find " + filename

    def update_mkv_merge(self, filename: str):
        for status in self.merge_status:
            if status.filename == filename:
                status.merged = True
                return None
        return self.name + " could not find " + filename

    def update_mkv_split(self, filename: str):
        for status in self.split_status:
            if status.filename == filename:
                status.split = True
                return None
        return self.name + " could not find " + filename

    def update_iso_mkv_count(self, filename: str, count: int) -> Optional[str]:
        for status in self.iso_status:
            if status.filename == filename:
                status.mkv_count = count
                return None
        return self.name + " could not find " + filename

    def mark_iso_mkv_extracted(self, filename: str) -> Optional[str]:
        for status in self.iso_status:
            if status.filename == filename:
                status.mkv_extracted = True
                return None
        return self.name + " could not find " + filename

    def get_mkv_converted_original(self, filename: str) -> Tuple[bool, Optional[str]]:
        for status in self.mkv_status:
            if status.filename == filename:
                return status.converted_original, None
        return False, self.name + " could not find " + filename

    def mark_mkv_converted_original(self, filename: str) -> Optional[str]:
        for status in self.mkv_status:
            if status.filename == filename:
                status.converted_original = True
                return None
        return self.name + " could not find " + filename

    def get_mkv_converted_tv(self, filename: str) -> Tuple[bool, Optional[str]]:
        for status in self.mkv_status:
            if status.filename == filename:
                return status.converted_tv, None
        return False, self.name + " could not find " + filename

    def mark_mkv_converted_tv(self, filename: str) -> Optional[str]:
        for status in self.mkv_status:
            if status.filename == filename:
                status.converted_tv = True
                return None
        return self.name + " could not find " + filename

    def get_mkv_converted_mobile(self, filename: str) -> Tuple[bool, Optional[str]]:
        for status in self.mkv_status:
            if status.filename == filename:
                return status.converted_mobile, None
        return False, self.name + " could not find " + filename

    def mark_mkv_converted_mobile(self, filename: str) -> Optional[str]:
        for status in self.mkv_status:
            if status.filename == filename:
                status.converted_mobile = True
                return None
        return self.name + " could not find " + filename

    def get_mkv_extracted_subtitles(self, filename: str) -> Tuple[bool, Optional[str]]:
        for status in self.mkv_status:
            if status.filename == filename:
                return status.extracted_subtitles, None
        return False, self.name + " could not find " + filename

    def mark_mkv_extracted_subtitles(self, filename: str) -> Optional[str]:
        for status in self.mkv_status:
            if status.filename == filename:
                status.extracted_subtitles = True
                return None
        return self.name + " could not find " + filename

    def save(self):
        data = dict()
        data["__DO_NOT_TOUCH__"] = "Warning! This file is auto generated, please do not touch."

        data["iso"] = []
        for iso in self.iso_status:
            temp = dict()
            temp["filename"] = iso.filename
            temp["last_checked"] = iso.last_checked.strftime('%m-%d-%Y %H:%M:%S')
            temp["mkv_extracted"] = iso.mkv_extracted
            temp["mkv_count"] = iso.mkv_count
            data["iso"].append(temp)

        data["merge"] = []
        for merge in self.merge_status:
            temp = dict()
            temp["filename"] = merge.filename
            temp["merged"] = merge.merged
            data["merge"].append(temp)

        data["split"] = []
        for split in self.split_status:
            temp = dict()
            temp["filename"] = split.filename
            temp["split"] = split.split
            data["split"].append(temp)

        data["mkv"] = []
        for mkv in self.mkv_status:
            temp = dict()
            temp["filename"] = mkv.filename
            temp["converted_original"] = mkv.converted_original
            temp["converted_tv"] = mkv.converted_tv
            temp["converted_mobile"] = mkv.converted_mobile
            temp["extracted_subtitles"] = mkv.extracted_subtitles
            data["mkv"].append(temp)

        with open(self.path, "w") as outfile:
            json.dump(data, outfile, indent=2)
