from datetime import datetime
from pathlib import Path
from typing import Tuple, List, Optional

from .data import Data, MergeData, SplitData
from .status import Status, MergeStatus, SplitStatus


def get_info_pairs(directory: Path) -> Tuple[List[Tuple[Data, Status]], Optional[str]]:
    if not isinstance(directory, Path):
        return [], "Given directory, {0}, is not a Path.".format(Path)

    glob = list(directory.rglob('data.json'))
    filtered_list = [x for x in glob if x is not None and "#recycle" not in str(x)]
    data_list = [Data(file) for file in filtered_list]

    return [(data, Status(data)) for data in data_list], None


def check_pairs_for_errors(data: List[Tuple[Data, Status]]) -> List[str]:
    messages: List = list()

    for pair in data:
        _data: Data = pair[0]
        _status: Status = pair[1]
        for error in _data.parse_errors:
            messages.append("Parse error: " + str(_data.path) + ", " + error)
        for error in _status.parse_errors:
            messages.append("Parse error: " + str(_status.path) + ", " + error)

    return messages


def get_iso_check_list(data: Data, status: Status) -> List[Path]:
    iso_list: List[Path] = []

    for iso in data.iso_data:
        if iso.has_error:
            continue

        for iso_status in status.iso_status:
            if iso.filename == iso_status.filename:
                diff = abs((iso_status.last_checked - datetime.now()).total_seconds())
                if diff > 15552000:
                    iso_list.append(status.path.parent / "Backup" / iso_status.filename)
                break

    return iso_list


def get_iso_mkv_extract_list(data: Data, status: Status) -> List[Path]:
    iso_list: List[Path] = []

    for iso in data.iso_data:
        if iso.has_error:
            continue

        for iso_status in status.iso_status:
            if iso.filename == iso_status.filename and not iso_status.mkv_extracted and not iso.check_only:
                iso_list.append(status.path.parent / "Backup" / iso_status.filename)
                break

    return iso_list


def get_mkv_merge_list(data: Data, status: Status) -> List[Tuple[MergeData, MergeStatus]]:
    merge_list: List[Tuple[MergeData, MergeStatus]] = []

    for merge in data.merge_data:
        if merge.has_error:
            continue

        for merge_status in status.merge_status:
            if merge.output_name == merge_status.filename and not merge_status.merged:
                merge_list.append((merge, merge_status))
                break

    return merge_list


def get_mkv_split_list(data: Data, status: Status) -> List[Tuple[SplitData, SplitStatus]]:
    split_list: List[Tuple[SplitData, SplitStatus]] = []

    for split in data.split_data:
        if split.has_error:
            continue

        for split_status in status.split_status:
            if split.filename == split_status.filename and not split_status.split:
                split_list.append((split, split_status))
                break

    return split_list


def get_mkv_mp4_convert_list(data: Data, status: Status) -> List[Path]:
    mkv_list: List[Path] = []

    for mkv in data.mkv_data:
        if mkv.has_error:
            continue

        for mkv_status in status.mkv_status:
            if mkv.filename == mkv_status.filename and (not mkv_status.converted_original or
                                                        not mkv_status.converted_tv or
                                                        not mkv_status.converted_mobile or
                                                        not mkv_status.extracted_subtitles):
                mkv_list.append(status.path.parent / "MKV" / mkv_status.filename)
                break

    return mkv_list


def get_delete_list(data: Data, status: Status) -> Tuple[str, List[Path]]:
    mkv_list: List[Path] = []

    name = data.name

    path = data.path.parent / "MKV"
    if not path.exists():
        return "", mkv_list

    if len(data.parse_errors) or len(status.parse_errors):
        return "Delete script: {} has parse errors.".format(name), mkv_list

    temp_mkv_list: List[str] = []
    for path_item in list(path.glob("**/*.mkv")):
        temp_mkv_list.append(str(path_item))

    mkv_count = len(temp_mkv_list)

    for merge_item in data.merge_data:
        for merge_file in merge_item.filenames:
            file_path = path / merge_file.filename
            file_path_string = str(file_path)
            if file_path_string in temp_mkv_list:
                temp_mkv_list.remove(file_path_string)

    for data_item in data.split_data:
        file_path = path / data_item.filename
        file_path_string = str(file_path)
        if file_path_string in temp_mkv_list:
            temp_mkv_list.remove(file_path_string)

    for data_item in data.mkv_data:
        file_path = path / data_item.filename
        file_path_string = str(file_path)
        if file_path_string in temp_mkv_list:
            temp_mkv_list.remove(file_path_string)

    for mkv_status_item in status.mkv_status:
        file_path = path / mkv_status_item.filename
        file_path_string = str(file_path)
        if file_path_string in temp_mkv_list:
            temp_mkv_list.remove(file_path_string)

    for item in temp_mkv_list:
        mkv_list.append(Path(item))

    new_mkv_count = len(mkv_list)
    if new_mkv_count > 0 and new_mkv_count == mkv_count:
        return "Delete script: delete script will not delete the entire directory for {}.".format(name), []

    return "", mkv_list
