import fnmatch
import json
import os
from dataclasses import dataclass, field
from pathlib import Path
from typing import List, Optional, Tuple

from .common import Resolution, VideoType


@dataclass
class IsoData:
    filename: str = ""
    check_only: bool = False
    custom_mkv_folder: str = ""
    has_error: bool = False


@dataclass
class MergeItem:
    filename: str = ""
    mkv_index: int = -1
    source: str = ""
    has_error: bool = False


@dataclass
class MergeData:
    filenames: List[MergeItem]
    output_name: str = ""
    has_error: bool = False


@dataclass
class SplitData:
    filename: str = ""
    mkv_index: int = -1
    source: str = ""
    chapters: str = ""
    has_error: bool = False


@dataclass
class MkvData:
    filename: str = ""
    mkv_index: int = -1
    resolution: Resolution = Resolution.invalid
    plex_name: str = ""
    source: str = ""
    has_error: bool = False


@dataclass
class Mp4Item:
    pattern: str = ""
    exclusions: List[str] = field(default_factory=list)
    type: VideoType = ""
    season: int = -1
    folder_name: str = ""


@dataclass
class Mp4Corrections:
    pattern: str = ""
    replacement: str = ""
    regex: str = ""


class Data:
    def __init__(self, path: Path):
        self.path: Path = path
        self.iso_data: List[IsoData] = []
        self.merge_data: List[MergeData] = []
        self.split_data: List[SplitData] = []
        self.mkv_data: List[MkvData] = []
        self.mp4_data_items: List[Mp4Item] = []
        self.mp4_data_corrections: List[Mp4Corrections] = []

        self.name: str = ""
        self.parse_errors = []

        self.__load_data()

    def __load_data(self):
        if not self.path.exists():
            return

        # noinspection PyBroadException
        try:
            with open(self.path, encoding='utf-8') as json_file:
                data = json.load(json_file)
        except Exception as _e:
            self.parse_errors.append("failed to parse json file {0}.".format(self.path))
            return

        if "name" not in data.keys():
            self.parse_errors.append("failed to find a name in json.")
            return
        elif not isinstance(data["name"], str):
            self.parse_errors.append("name is an invalid type.")
            return
        else:
            self.name = data["name"]

        if "iso" in data.keys():
            if not isinstance(data["iso"], list):
                self.parse_errors.append("iso is an invalid type.")
            else:
                for count, item in enumerate(data["iso"]):
                    has_error = False
                    if "filename" not in item.keys():
                        self.parse_errors.append("failed to find filename in iso #{0}.".format(count + 1))
                        item["filename"] = ""
                        has_error = True
                    elif not isinstance(item["filename"], str) or ".iso" not in item["filename"]:
                        self.parse_errors.append("filename in iso #{0} is invalid.".format(count + 1))
                        item["filename"] = ""
                        has_error = True
                    elif not (self.path.parent / "Backup" / item["filename"]).exists():
                        self.parse_errors.append("filename in iso #{0} is does not exist.".format(count + 1))
                        has_error = True

                    if "check_only" not in item.keys():
                        item["check_only"] = False
                    elif not isinstance(item["check_only"], bool):
                        self.parse_errors.append("check_only in iso #{0} is invalid.".format(count + 1))
                        item["check_only"] = False
                        has_error = True

                    if "custom_mkv_folder" not in item.keys():
                        item["custom_mkv_folder"] = ""
                    elif "custom_mkv_folder" in item.keys() and not isinstance(item["custom_mkv_folder"], str):
                        self.parse_errors.append("custom_mkv_folder in iso #{0} is invalid.".format(count + 1))
                        item["custom_mkv_folder"] = ""
                        has_error = True

                    self.iso_data.append(
                        IsoData(item["filename"], item["check_only"], item["custom_mkv_folder"], has_error))

        if "merge" in data.keys():
            if not isinstance(data["merge"], list):
                self.parse_errors.append("merge is an invalid type.")
            else:
                for count, item in enumerate(data["merge"]):
                    is_merge_item_error = False
                    if "filenames" not in item.keys():
                        self.parse_errors.append("failed to find filenames in merge #{0}.".format(count + 1))
                        item["filenames"] = []
                        is_merge_item_error = True
                    elif not isinstance(item["filenames"], list):
                        self.parse_errors.append("filenames in merge #{0} is invalid.".format(count + 1))
                        item["filenames"] = []
                        is_merge_item_error = True

                    if "output_name" not in item.keys():
                        self.parse_errors.append("failed to find output_name in merge #{0}.".format(count + 1))
                        item["output_name"] = ""
                        is_merge_item_error = True
                    elif not isinstance(item["output_name"], str) or ".mkv" not in item["output_name"]:
                        self.parse_errors.append("output_name in merge #{0} is invalid.".format(count + 1))
                        item["output_name"] = ""
                        is_merge_item_error = True

                    merge_list: List[MergeItem] = []
                    merge_files: List[str] = []

                    for merge_count, merge_item in enumerate(item["filenames"]):
                        has_error = False
                        if "filename" not in merge_item.keys():
                            self.parse_errors.append(
                                "failed to find filename in merge #{0} item #{1}.".format(count + 1, merge_count + 1))
                            merge_item["filename"] = ""
                            has_error = True
                            is_merge_item_error = True
                        elif not isinstance(merge_item["filename"], str) or ".mkv" not in merge_item["filename"]:
                            self.parse_errors.append(
                                "filename in merge #{0} item #{1} is invalid.".format(count + 1, merge_count + 1))
                            merge_item["filename"] = ""
                            has_error = True
                            is_merge_item_error = True
                        elif not (self.path.parent / "MKV" / merge_item["filename"]).exists():
                            self.parse_errors.append(
                                "filename in merge #{0} item #{1} does not exist.".format(count + 1, merge_count + 1))
                            has_error = True
                            is_merge_item_error = True

                        if "source" not in merge_item.keys():
                            merge_item["source"] = ""
                        elif not isinstance(merge_item["source"], str) or merge_item["source"] != "" and ".iso" not in \
                                merge_item["source"]:
                            self.parse_errors.append(
                                "source in merge #{0} item #{1} is invalid.".format(count + 1, merge_count + 1))
                            merge_item["source"] = ""
                            has_error = True
                            is_merge_item_error = True
                        elif merge_item["source"] != "" and not \
                                (self.path.parent / "BACKUP" / merge_item["source"]).exists():
                            has_error = True
                            is_merge_item_error = True

                        if merge_item["filename"] != "":
                            merge_files.append(merge_item["filename"])

                        mkv_index, mkv_index_error = self.get_mkv_index(merge_item["filename"],
                                                                        merge_item["source"])
                        if mkv_index_error != "":
                            self.parse_errors.append(
                                "filename in merge #{0} item #{1} "
                                "is invalid with error {2}.".format(count + 1,
                                                                    merge_count + 1,
                                                                    mkv_index_error))
                            is_merge_item_error = True
                        else:
                            merge_list.append(
                                MergeItem(merge_item["filename"], mkv_index, merge_item["source"], has_error))

                    if item["output_name"] in merge_files:
                        self.parse_errors.append("output_name in merge #{0} exists in filenames.".format(count + 1))
                        is_merge_item_error = True

                    self.merge_data.append(MergeData(merge_list, item["output_name"], is_merge_item_error))

        if "split" in data.keys():
            if not isinstance(data["split"], list):
                self.parse_errors.append("split is an invalid type.")
            else:
                for count, item in enumerate(data["split"]):
                    has_error: bool = False
                    if "filename" not in item.keys():
                        self.parse_errors.append("failed to find filename in split #{0}.".format(count + 1))
                        item["filename"] = ""
                        has_error = True
                    elif not isinstance(item["filename"], str) or ".mkv" not in item["filename"]:
                        self.parse_errors.append("filename in split #{0} is invalid.".format(count + 1))
                        item["filename"] = ""
                        has_error = True
                    elif not (self.path.parent / "MKV" / item["filename"]).exists():
                        self.parse_errors.append("filename in split #{0} does not exist.".format(count + 1))
                        has_error = True

                    if "source" not in item.keys():
                        item["source"] = ""
                    elif not isinstance(item["source"], str) or (item["source"] != "" and ".iso" not in item["source"]):
                        self.parse_errors.append("source in split #{0} is invalid.".format(count + 1))
                        item["filename"] = ""
                        has_error = True
                    elif item["source"] != "" and not (self.path.parent / "BACKUP" / item["source"]).exists():
                        self.parse_errors.append("source in split #{0} does not exist.".format(count + 1))
                        has_error = True

                    if "chapters" not in item.keys():
                        self.parse_errors.append("failed to find chapters in split #{0}.".format(count + 1))
                        item["chapters"] = ""
                        has_error = True
                    elif not isinstance(item["chapters"], str) or item["chapters"] == "":
                        self.parse_errors.append("chapters in split #{0} is invalid.".format(count + 1))
                        item["chapters"] = ""
                        has_error = True

                    mkv_index, mkv_index_error = self.get_mkv_index(item["filename"], item["source"])
                    if mkv_index_error != "":
                        self.parse_errors.append(
                            "filename in split #{0} is invalid with error {1}.".format(count + 1,
                                                                                       mkv_index_error))
                        continue

                    self.split_data.append(
                        SplitData(item["filename"], mkv_index, item["source"], item["chapters"], has_error))

        if "mkv" in data.keys():
            if not isinstance(data["mkv"], list):
                self.parse_errors.append("mkv is an invalid type.")
            else:
                plex_names: List[str] = []

                for count, item in enumerate(data["mkv"]):
                    has_error: bool = False
                    if "filename" not in item.keys():
                        self.parse_errors.append("failed to find filename in mkv #{0}.".format(count + 1))
                        item["filename"] = ""
                        has_error = True
                    elif not isinstance(item["filename"], str) or ".mkv" not in item["filename"]:
                        self.parse_errors.append("filename in mkv #{0} is invalid.".format(count + 1))
                        item["filename"] = ""
                        has_error = True
                    elif not (self.path.parent / "MKV" / item["filename"]).exists():
                        self.parse_errors.append("filename in mkv #{0} does not exist.".format(count + 1))
                        has_error = True

                    if "source" not in item.keys():
                        item["source"] = ""
                    elif not isinstance(item["source"], str) or (item["source"] != "" and ".iso" not in item["source"]):
                        self.parse_errors.append("source in mkv #{0} is invalid.".format(count + 1))
                        item["source"] = ""
                        has_error = True
                    elif item["source"] != "" and not (self.path.parent / "BACKUP" / item["source"]).exists():
                        self.parse_errors.append("source in mkv #{0} does not exist.".format(count + 1))
                        has_error = True

                    if "resolution" not in item.keys():
                        self.parse_errors.append("failed to find resolution in mkv #{0}.".format(count + 1))
                        item["resolution"] = ""
                        has_error = True
                    elif Resolution(item["resolution"]) == Resolution.invalid:
                        self.parse_errors.append("resolution in mkv #{0} is invalid.".format(count + 1))
                        item["resolution"] = ""
                        has_error = True

                    if "plex_name" not in item.keys():
                        self.parse_errors.append("failed to find plex_name in mkv #{0}.".format(count + 1))
                        item["plex_name"] = ""
                        has_error = True
                    elif not isinstance(item["plex_name"], str) or item["plex_name"] == "":
                        self.parse_errors.append("plex_name in mkv #{0} is invalid.".format(count + 1))
                        item["plex_name"] = ""
                        has_error = True

                    mkv_index, mkv_index_error = self.get_mkv_index(item["filename"], item["source"])
                    if mkv_index_error != "":
                        self.parse_errors.append(
                            "filename in mkv #{0} is invalid with error {1}.".format(count + 1,
                                                                                     mkv_index_error))
                        continue

                    if item["plex_name"] in plex_names:
                        self.parse_errors.append(
                            "plex_name in mkv #{0} is a duplicate.".format(count + 1))
                        continue

                    plex_names.append(item["plex_name"])

                    self.mkv_data.append(
                        MkvData(item["filename"], mkv_index, Resolution(item["resolution"]), item["plex_name"],
                                item["source"], has_error))

        if "mp4" in data.keys():
            if not isinstance(data["mp4"], dict):
                self.parse_errors.append("mp4 is an invalid type.")
            else:
                if "items" in data["mp4"].keys():
                    if not isinstance(data["mp4"]["items"], list):
                        self.parse_errors.append("mp4 items is an invalid type.")
                    else:
                        for count, item in enumerate(data["mp4"]["items"]):
                            if "type" not in item.keys():
                                self.parse_errors.append("failed to find type in mp4 item #{0}.".format(count + 1))
                                continue

                            video_type = VideoType(item["type"])

                            if "pattern" not in item.keys():
                                self.parse_errors.append("failed to find pattern in mp4 item #{0}.".format(count + 1))
                            elif not isinstance(item["pattern"], str):
                                self.parse_errors.append(
                                    "pattern in mp4 item #{0} is invalid.".format(count + 1))
                            elif "folder_name" not in item.keys():
                                self.parse_errors.append(
                                    "failed to find folder_name in mp4 item #{0}.".format(count + 1))
                            elif not isinstance(item["folder_name"], str):
                                self.parse_errors.append(
                                    "folder_name in mp4 items #{0} is invalid.".format(count + 1))
                            elif video_type == VideoType.invalid:
                                self.parse_errors.append("type in mp4 items #{0} is an invalid type.".format(count + 1))
                            elif (video_type == VideoType.tv or video_type == VideoType.docuseries) \
                                    and "season" not in item.keys():
                                self.parse_errors.append("failed to find season in mp4 items #{0}.".format(count + 1))
                            elif (video_type == VideoType.tv or video_type == VideoType.docuseries) \
                                    and not isinstance(item["season"], int) \
                                    and item["season"] >= 0:
                                self.parse_errors.append(
                                    "season in mp4 items #{0} is an invalid type.".format(count + 1))
                            else:
                                exclusions = []
                                if "exclusions" in item.keys():
                                    if not isinstance(item["exclusions"], list):
                                        self.parse_errors.append(
                                            "exclusions in mp4 item #{0} is invalid.".format(count + 1))
                                        continue

                                    if not all(isinstance(s, str) for s in item["exclusions"]):
                                        self.parse_errors.append(
                                            "exclusions in mp4 item #{0} is invalid, must all be strings.".format(count + 1))
                                        continue

                                    exclusions = item["exclusions"]

                                mp4_path = self.path.parent / "MP4"
                                if item["pattern"] == "":
                                    pattern = "*"
                                else:
                                    pattern = "*" + item["pattern"] + "*"

                                if len(list(mp4_path.glob(pattern))) == 0:
                                    self.parse_errors.append(
                                        "no files matching pattern in mp4 items #{0}.".format(count + 1))
                                    continue

                                season = -1
                                if video_type == VideoType.tv or video_type == VideoType.docuseries:
                                    season = item["season"]

                                self.mp4_data_items.append(Mp4Item(item["pattern"], exclusions, video_type,
                                                                   season, item["folder_name"]))

                if "corrections" in data["mp4"].keys():
                    if not isinstance(data["mp4"]["corrections"], list):
                        self.parse_errors.append("mp4 corrections is an invalid type.")
                    else:
                        for count, item in enumerate(data["mp4"]["corrections"]):
                            if "pattern" not in item.keys():
                                self.parse_errors.append(
                                    "failed to find pattern in mp4 correction #{0}.".format(count + 1))
                            elif not isinstance(item["pattern"], str):
                                self.parse_errors.append(
                                    "pattern in mp4 corrections #{0} is invalid.".format(count + 1))
                            elif "replacement" not in item.keys():
                                self.parse_errors.append(
                                    "failed to find pattern in mp4 correction #{0}.".format(count + 1))
                            elif not isinstance(item["replacement"], str):
                                self.parse_errors.append(
                                    "replacement in mp4 corrections #{0} is invalid.".format(count + 1))
                            else:
                                regex_string = ""
                                if "*" in item["pattern"]:
                                    regex_string = fnmatch.translate(item["pattern"])

                                self.mp4_data_corrections.append(Mp4Corrections(item["pattern"],
                                                                                item["replacement"],
                                                                                regex_string))
        else:
            self.parse_errors.append("failed to find mp4 data.")

    @staticmethod
    def get_mkv_index(filename: str, source: str) -> Tuple[int, str]:
        if filename == "" or source == "":
            return -1, ""

        basename, ext = os.path.splitext(filename)
        if ext != ".mkv":
            return -1, "failed to find .mkv in {}".format(filename)

        if basename.find("_t") == -1:
            return -1, "failed to find _t in {}".format(filename)

        title = basename.split("_t")[1]
        try:
            return int(title), ""
        except ValueError:
            return -1, "failed to convert {} to int in {}".format(title, filename)

    def get_original_iso_filename(self, filename: str) -> Tuple[str, Optional[str]]:
        filename_lower = filename.lower()
        for data in self.iso_data:
            if data.filename.lower() == filename_lower:
                return data.filename, None
        return "", self.name + " could not find " + filename

    def get_mkv_list_from_iso_filename(self, filename: str) -> Tuple[List[str], List[str]]:
        filename_lower = filename.lower()
        mkv_list: List[str] = []

        for mkv_item in self.mkv_data:
            if mkv_item.source.lower() == filename_lower:
                mkv_list.append(mkv_item.filename)

        for merge_item in self.merge_data:
            for mkv_sub_item in merge_item.filenames:
                if mkv_sub_item.source.lower() == filename_lower:
                    mkv_list.append(mkv_sub_item.filename)

        for split_item in self.split_data:
            if split_item.source.lower() == filename_lower:
                mkv_list.append(split_item.filename)

        mkv_missing_list: List[str] = []
        custom, _ = self.get_iso_custom_mkv_folder(filename)
        mkv_path = self.path.parent / "MKV"
        if mkv_path.exists():
            for mkv_item in mkv_list:
                path = mkv_path / mkv_item
                if not path.exists():
                    mkv_missing_list.append(mkv_item)
        else:
            mkv_missing_list = mkv_list.copy()

        return mkv_missing_list, mkv_list

    def get_original_mkv_merge_filename(self, filename: str) -> Tuple[str, Optional[str]]:
        filename_lower = filename.lower()
        for data in self.merge_data:
            if data.output_name.lower() == filename_lower:
                return data.output_name, None
        return "", self.name + " could not find " + filename

    def get_original_mkv_split_filename(self, filename: str) -> Tuple[str, Optional[str]]:
        filename_lower = filename.lower()
        for data in self.split_data:
            if data.filename.lower() == filename_lower:
                return data.filename, None
        return "", self.name + " could not find " + filename

    def get_original_mkv_filename(self, filename: str) -> Tuple[str, Optional[str]]:
        filename_lower = filename.lower()
        for data in self.mkv_data:
            if data.filename.lower() == filename_lower:
                return data.filename, None
        return "", self.name + " could not find " + filename

    def get_iso_custom_mkv_folder(self, filename: str) -> Tuple[str, Optional[str]]:
        filename_lower = filename.lower()
        for data in self.iso_data:
            if data.filename.lower() == filename_lower:
                return data.custom_mkv_folder, None
        return "", self.name + " could not find " + filename

    def get_mkv_resolution(self, filename: str) -> Tuple[Resolution, Optional[str]]:
        filename_lower = filename.lower()
        for data in self.mkv_data:
            if data.filename.lower() == filename_lower:
                return data.resolution, None
        return Resolution.invalid, self.name + " could not find " + filename

    def get_mkv_plex_name(self, filename: str) -> Tuple[str, Optional[str]]:
        filename_lower = filename.lower()
        for data in self.mkv_data:
            if data.filename.lower() == filename_lower:
                return data.plex_name, None
        return "", self.name + " could not find " + filename
