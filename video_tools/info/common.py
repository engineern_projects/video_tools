from enum import Enum, auto


class Resolution(Enum):
    invalid = 1
    r480p = 2
    mobile = 3
    r720p = 4
    tv = 5
    r1080p = 6
    r2160p = 7

    @classmethod
    def _missing_(cls, value):
        for member in cls:
            if member.name == value:
                return member

        return Resolution.invalid


class VideoType(Enum):
    invalid = 1
    movie = 2
    tv = 3
    documentary = 4
    docuseries = 5

    @classmethod
    def _missing_(cls, value):
        for member in cls:
            if member.name == value:
                return member

        return VideoType.invalid
