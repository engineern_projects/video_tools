from typing import List, Tuple

subtitle_language_dict = {
    "ara": "ara",
    "bul": "bul",
    "cat": "cat",
    "ces": "cze",
    "chi": "chi",
    "cze": "cze",
    "dan": "dan",
    "deu": "ger",
    "dut": "dut",
    "ell": "ell",
    "en": "eng",
    "en-gb": "eng",
    "en-us": "eng",
    "eng": "eng",
    "est": "est",
    "fin": "fin",
    "fra": "fre",
    "fre": "fre",
    "ger": "ger",
    "gre": "gre",
    "heb": "heb",
    "hin": "hin",
    "hun": "hun",
    "hrv": "hrv",
    "ice": "ice",
    "ind": "ind",
    "isl": "ice",
    "ita": "ita",
    "jpn": "jpn",
    "kor": "kor",
    "lav": "lav",
    "lit": "lit",
    "nld": "dut",
    "nor": "nor",
    "may": "may",
    "msa": "may",
    "pol": "pol",
    "por": "por",
    "ron": "rum",
    "rum": "rum",
    "rus": "rus",
    "slk": "slo",
    "slo": "slo",
    "slv": "slv",
    "spa": "spa",
    "srp": "srp",
    "swe": "swe",
    "tel": "tel",
    "tha": "tha",
    "tam": "tam",
    "tur": "tur",
    "ukr": "ukr",
    "vie": "vie",
    "zho": "chi"
}

subtitle_language_replace_dict = {}


def __expand_language_replace_dict():
    global subtitle_language_dict, subtitle_language_replace_dict

    subtitle_extras = [
        ["cc", "pre", ""],
        ["cc", "post", ""],
        ["forced", "post", "forced"]
    ]

    subtitle_language_dict_temp = {}

    for key in subtitle_language_dict:
        new_key = "." + key
        value = "." + subtitle_language_dict[key]
        subtitle_language_dict_temp[new_key] = value

        for extra in subtitle_extras:
            if extra[1] == "pre":
                temp_key = "." + extra[0] + new_key
                if extra[2] == "":
                    temp_value = value
                else:
                    temp_value = "." + extra[2] + value
                subtitle_language_dict_temp[temp_key] = temp_value
            else:
                temp_key = new_key + "." + extra[0]
                if extra[2] == "":
                    temp_value = value
                else:
                    temp_value = value + "." + extra[2]
                subtitle_language_dict_temp[temp_key] = temp_value

    subtitle_language_replace_dict = subtitle_language_dict_temp
    subtitle_language_dict_temp = {}

    for key in subtitle_language_replace_dict:
        subtitle_language_dict_temp[key + ".srt"] = subtitle_language_replace_dict[key] + ".srt"

    subtitle_language_replace_dict = subtitle_language_dict_temp


def valid_suffixes(suffixes: List[str]) -> Tuple[bool, str, str]:
    is_valid = True
    return_suffixes = []
    return_string = ""
    error_string = ""

    if len(suffixes) < 2:
        is_valid = False
        error_string = "Invalid number of suffixes given."
        return is_valid, return_string, error_string

    for suffix in suffixes:
        suffix = suffix.lower()
        if suffix == ".cc" or suffix == ".forced" or suffix == ".srt":
            return_suffixes.append(suffix)
        elif len(suffix) == 37 and len(suffix.split("-")) == 5:
            is_valid = False
        elif len(suffix) > 2 and suffix[1] == "#" and suffix[2:].isdigit():
            is_valid = False
        elif suffix == ".orig":
            is_valid = False
        elif suffix[1:] in subtitle_language_dict.keys():
            return_suffixes.append("." + subtitle_language_dict[suffix[1:]])

    if is_valid:
        temp_suffix = "".join(return_suffixes)

        if temp_suffix not in subtitle_language_replace_dict.keys():
            is_valid = False
            error_string = "Could not find '" + temp_suffix + "'."
        else:
            return_string = temp_suffix

    return is_valid, return_string, error_string


__expand_language_replace_dict()
