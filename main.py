import argparse

from video_tools.cmd import check_iso_hash, iso_to_mkv, mkv_to_mp4, mkv_cleanup, update_status, mkv_merge, mkv_split, \
    file_organizer


def main():
    parser = argparse.ArgumentParser()
    sub_parser = parser.add_subparsers(title="command", description="Commands that can be ran.", dest="command")
    check_iso_hash.generate_parser(sub_parser)
    iso_to_mkv.generate_parser(sub_parser)
    file_organizer.generate_parser(sub_parser)
    mkv_merge.generate_parser(sub_parser)
    mkv_split.generate_parser(sub_parser)
    mkv_to_mp4.generate_parser(sub_parser)
    mkv_cleanup.generate_parser(sub_parser)
    update_status.generate_parser(sub_parser)

    args = parser.parse_args()
    if args.command is None:
        parser.print_help()
    else:
        args.func(args)


if __name__ == '__main__':
    main()
